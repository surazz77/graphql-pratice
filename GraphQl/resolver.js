const Event = require("../Models/event.js");
const User = require("../Models/user.js");
const bcrypt = require("bcryptjs");

const event = async eventIds =>{
  try{
    const events = await event.find({_id:{$in:eventIds}})
    events.map(event=>{
      return{
        ...event._doc,
        _id:event.id,
        creater:user.bind(this,event.creater)
      }
    });
    return event
  }
  catch(err){
    console.log(err)
    throw err
  }
}

const user = async userId =>{
  try{
    const user = await User.findById(userId)
    return {
      ...user._doc,
      _id:user.id,
      createdEvent: events.bind(this,user._doc.createdEvent)
    }
  }
  catch(err){
    console.log(err)
    throw err
  }
}

module.exports = {
  events: async () => {
    try {
      const events = await Event.find();
      return events.map(event => {
        return { ...event._doc, _id: event.id,creater:user.bind(this,event._doc.creater) };
      });
    } catch (err) {
      throw err;
    }
  },
  createEvent: async args => {
    const event = new Event({
      title: args.eventInput.title,
      description: args.eventInput.description,
      price: +args.eventInput.price,
      date: args.eventInput.date,
      creator: "5cdd8a6b12f13839316861c5"
    });
    let createdEvent;
    try {
      const result = await event.save();
      createdEvent = { ...result._doc, _id: result._doc._id.toString() };
      const creater = await User.findById("5cdd8a6b12f13839316861c5");
      if (!creater) {
        throw new Error("User not found.");
      }
      creater.createdEvents.push(event);
      await creater.save();
      return createdEvent;
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
  users: async () => {
    try {
      const users = await User.find();
      return users.map(user => {
        return { ...user._doc, _id: user.id };
      });
    } catch (err) {
      console.log(err);
      throw err;
    }
  },
  createUser: async args => {
    try {
      const existingUser = await User.findOne({ email: args.userInput.email });
      if (existingUser) {
        throw new Error("User exists already.");
      }
      const hashedPassword = bcrypt.hash(args.userInput.password, 12);
      const user = new User({
        email: args.userInput.email,
        password: hashedPassword
      });
      const result = user.save();
      return { ...result._doc, _id: result.id, password: null };
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
};
