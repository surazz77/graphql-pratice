const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const graphqlHttp = require("express-graphql");
const { buildSchema } = require("graphql");
const mongoose = require("mongoose");
const Event = require("./Models/event.js");
const User = require("./Models/user.js");
const bcrypt = require("bcryptjs");
const graphQlSchema = require("./GraphQl/schema.js")
const graphQlResolver = require('./GraphQl/resolver')

app.use(bodyParser.json());

app.use(
  "/graphql",
  graphqlHttp({
    schema: graphQlSchema,
    rootValue:graphQlResolver,
    graphiql: true
  })
);

mongoose
  .connect(
    "mongodb://surajdhakal:suraj463093@ds125385.mlab.com:25385/mydatabase"
  )
  .then(() => {
    app.listen(3000);
  })
  .catch(err => {
    console.log(err);
  });
